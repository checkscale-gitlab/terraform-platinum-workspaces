locals {
  gitlab_oauth_token_id = "ot-uea8Bt3r7P16GD7k"
}

resource "tfe_workspace" "platinum" {
  name         = local.platinum_name
  organization = data.tfe_organization.platinum_org.name
  tag_names    = ["terraform:managed", "hcloud"]
  vcs_repo {
    identifier     = gitlab_project.platinum.path_with_namespace
    branch         = "main"
    oauth_token_id = local.gitlab_oauth_token_id
  }
}
