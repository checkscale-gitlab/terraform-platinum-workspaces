
variable "tfe_api_token" {
  type      = string
  sensitive = true
}

variable "hcloud_token" {
  type      = string
  sensitive = true
}

variable "gitlab_access_token" {
  type      = string
  sensitive = true
}

variable "cloudflare_token" {
  type = string
  sensitive = true
}